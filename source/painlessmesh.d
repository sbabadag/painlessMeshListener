module painlessmesh;

import painlessjson;

import vibe.appmain;
import vibe.core.core : runTask, sleep;
import vibe.core.log;
import vibe.core.net : TCPConnection, listenTCP;
import vibe.stream.operations : readLine;

import core.time;

void connectTCPAsync(string host, ushort port, void delegate(TCPConnection) del)
{
    import vibe.core.net : connectTCP;
    runTask({
        try {
            auto c = connectTCP(host, port);
            del(c);
        } catch (Exception e) {
			logError("Unable to connect to TCPServer", e.msg);
        }
    });
}

class Node {
public:
    size_t nodeId;
    Connection[size_t] connections;

    void delegate(size_t from, JSONValue msg) receive;
}

void initMaintenance(Node node) {
    runTask(() {
        while(true) {
            auto currT = MonoTime.currTime;
            logDebug("Checking for unused connections");
            foreach(fromID, conn; node.connections) {
                node.sendNodeSync(fromID);
                if (currT - conn.lastReceived > dur!"seconds"(60))
                        node.removeConnection(fromID);
            }
            sleep(6.seconds());
        }
    });
}

void initConnection(Node node, TCPConnection conn) {
    import std.conv : to;
    logDebug("New connection");
    size_t fromID = 0;
    try {
        while (!conn.empty) {
            auto json = conn.readJsonObject();
            logDebug("Received message: %s", json);
            if (fromID == 0 && (
                    json["type"].integer == packageType.TIME_SYNC ||
                    json["type"].integer == 
                        packageType.NODE_SYNC_REQUEST ||
                    json["type"].integer == 
                        packageType.NODE_SYNC_REPLY)
                ) {
                fromID = json["from"].integer.to!size_t;
                Connection meshConnection;
                meshConnection.tcp = conn;
                meshConnection.nodeId = fromID;
                node.connections[fromID] = meshConnection;
            }

            if (fromID in node.connections) {
                node.connections[fromID].lastReceived = MonoTime.currTime;
            }

            node.handleMessage(fromID, json);
        }
    } catch (Exception e) {
        logError("Failed to read from client: %s", e.msg);
    }
    node.removeConnection(fromID);
}

void initServer(Node node, size_t port) {
    import std.conv : to;
	listenTCP(port.to!ushort, (TCPConnection conn) {node.initConnection(conn);});
    node.initMaintenance;
}

import vibe.core.net : NetworkAddress, connectTCP;
void connectTCPAsync(in ref NetworkAddress addr, void delegate(TCPConnection) del)
{
    runTask({
        try {
            logDebug("Trying to connect to: %s", addr.toString);
            auto c = connectTCP(addr);
            del(c);
        } catch (Exception e) {
            logDebug("Unable to connect to: %s. Retrying in 10 seconds", addr.toString);
            sleep(10.seconds());
            connectTCPAsync(addr, del);
        }
    });
}

void initClient(Node node, in ref NetworkAddress addr) {
	connectTCPAsync(addr, (TCPConnection conn) {node.initConnection(conn);});
    node.initMaintenance;
}

void onReceive(Node node, void delegate(size_t from, JSONValue msg) callback) {
    node.receive = callback;
}


bool sendSingle(Node node, size_t dest, string msg) {
    JSONValue[string] pack;
    pack["dest"] = dest.toJSON;
    pack["from"] = node.nodeId.toJSON;
    pack["type"] = packageType.SINGLE.toJSON;
    pack["msg"] = msg;

    foreach(k, v; node.connections) {
        if (v.containsId(dest)) {
            node.safeWrite(k, pack.toJSON);
            return true;
        }
    }
    return false;
}

bool sendBroadcast(Node node, string msg) {
    JSONValue[string] pack;
    pack["dest"] = 0.toJSON;
    pack["from"] = node.nodeId.toJSON;
    pack["type"] = packageType.BROADCAST.toJSON;
    pack["msg"] = msg;

    foreach(k, v; node.connections) {
        logDebug("sendBroadcast: send message to %u.", k); 
        node.safeWrite(k, pack.toJSON);
    }
    return true;
}

struct subConnection {
    size_t nodeId;
    subConnection[] subs;
}

struct Connection {
    @SerializeIgnore TCPConnection tcp;
    size_t nodeId;
    subConnection[] subs;

    @SerializeIgnore MonoTime lastReceived;
}

bool containsId(C)(C conn, size_t id) {
    import std.range : empty;
    import std.algorithm : any;
    if (conn.nodeId == id)
        return true;
    else if (conn.subs.empty)
        return false;
    else
        return conn.subs.any!((c) => c.containsId(id));
}

bool safeWrite(Node node, size_t connectionId, JSONValue pkge) {
    import std.conv : to;
    logDebug("Send to %u, msg %s", connectionId, pkge.to!string);
    try {
        if (connectionId in node.connections) {
            node.connections[connectionId].tcp.write(pkge.to!string);
            node.connections[connectionId].tcp.write(['\0']); // append a zero character
            return true;
        } else
            return false;
    } catch (Exception e) {
        node.removeConnection(connectionId);
        logError("Failed to write to client: %s", e.msg);
        return false;
    }
}

bool removeConnection(Node node, size_t connId) {
    if (connId in node.connections) {
        logDebug("Removing connection %u: ", connId);
		node.connections[connId].tcp.close();
        node.connections.remove(connId);
        // Inform the other nodes
        foreach (otherID; node.connections.keys) {
            node.sendNodeSync(otherID, true);
        }
        return true;
    } else
        return false;
}

enum packageType {
    DROP                    = 3,
    TIME_SYNC               = 4,
    NODE_SYNC_REQUEST       = 5,
    NODE_SYNC_REPLY         = 6,
    BROADCAST               = 8,  //application data for everyone
    SINGLE                  = 9   //application data for a single node
};

auto readJsonObject(T)(ref T connection)
{
    import vibe.stream.operations : readUntil;
    import std.range : walkLength;
    import std.algorithm : balancedParens, filter;
    import std.json : parseJSON;

    bool firstRun = true;
    connection.readUntil([123]); // Find the starting {
    string line = "{";
    int countOpen = 1;
    while(countOpen > 0 || firstRun)
    {
        auto segment = cast(string) connection.readUntil([125]);
        auto newOpen = segment.filter!((a) => a == '{').walkLength;
        countOpen += newOpen;
        line ~= segment ~ "}";
        --countOpen;
        firstRun = false;
    }
    assert(line.balancedParens('{', '}'), "Object not complete");
    return line.parseJSON.object;
}

struct timeSync {
    uint type;
    uint t0;
    uint t1;
    uint t2;
}

import std.json : JSONValue;
auto handleMessage(Node node, size_t fromID, JSONValue[string] json) {
    if (fromID !in node.connections) {
        logDebug("Unknown nodeId");
        return false;
    }
    if (json["type"].integer == packageType.NODE_SYNC_REQUEST ||
            json["type"].integer == packageType.NODE_SYNC_REPLY ) {
        node.connections[fromID].subs = json["subs"].fromJSON!(subConnection[]);
        if (json["type"].integer == packageType.NODE_SYNC_REQUEST)
            node.sendNodeSync(fromID, false);
    } else if (json["type"].integer == packageType.TIME_SYNC) {
        auto timeReceived = nodeTime();
        auto tS = json["msg"].fromJSON!timeSync;
        if (tS.type != 2) {
            JSONValue[string] pack;
            pack["dest"] = fromID.toJSON;
            pack["from"] = node.nodeId.toJSON;
            pack["type"] = packageType.TIME_SYNC.toJSON;
            if (tS.type == 0) {
                tS.t0 = nodeTime();
            } else if (tS.type == 1) {
                tS.t1 = timeReceived;
                tS.t2 = nodeTime();
            }
            ++tS.type;
            pack["msg"] = tS.toJSON;
            node.safeWrite(fromID, pack.toJSON);
        } else {
            adjustTime += ((tS.t1 - tS.t0)/2 + (tS.t2-timeReceived)/2);
        }
    } else if (json["type"].integer == packageType.BROADCAST) {
        // if broadcast, forward it and pass it to received callback
        foreach(k, v; node.connections) {
            if (!v.containsId(fromID)) { // Don't send it back
                node.safeWrite(k, json.toJSON);
            }
        }
        if (node.receive)
            node.receive(json["from"].fromJSON!size_t, json["msg"]);
    } else if (json["type"].integer == packageType.SINGLE) {
        auto dest = json["dest"].fromJSON!size_t;
        if (node.receive && dest == node.nodeId) {
            node.receive(json["from"].fromJSON!size_t, json["msg"]);
        } else {
            // Forward it
            foreach(k, v; node.connections) {
                if (v.containsId!(typeof(v))(dest)) {
                    //forwarding
                    node.safeWrite(k, json.toJSON);
                    break;
                }
            }
        }
    }
    return true;
}

void sendNodeSync(Node node, size_t fromID, bool request = true) {
    import std.range : byPair, array;
    import std.algorithm : filter, map;
    if (fromID in node.connections) {
        JSONValue[string] pack;
        pack["dest"] = fromID.toJSON;
        pack["from"] = node.nodeId.toJSON;
        if (request)
            pack["type"] = packageType.NODE_SYNC_REQUEST.toJSON;
        else
            pack["type"] = packageType.NODE_SYNC_REPLY.toJSON;
        pack["subs"] = node.connections.byPair.filter!((t) => t[0] != fromID).map!((t) => t[1]).array.toJSON;
        node.safeWrite(fromID, pack.toJSON);
    } else {
        logDebug("Connection not found during node sync");
    }
}

uint adjustTime = 0;

uint nodeTime() {
    return cast(uint) (((MonoTime.currTime-MonoTime(0)).total!"usecs") + adjustTime);
}
